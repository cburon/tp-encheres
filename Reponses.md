#Enchère au premier prix et enchère de Vickrey
Dans cette seconde partie du TP, nous allons étudier des modifications (simples) de la stratégie des agents sur le score et l’épargne de l’agent.

##Enchère scellée au premier prix

Commencez par vous rendre dans la classe TargetAgent. La fonction computeBid permet de définir l’enchère que fera l’agent. Essayez d’augmenter et de diminuer artificiellement la valeur de ces enchères. Qu’observez-vous ?

<!--Votre réponse-->

##Enchère de Vickrey

Jusqu’à présent, le protocole utilisé par les agents est l’enchère scellée au premier prix : chaque agent soumet une unique enchère. Celui ayant fait la meilleure offre gagne et paie ce qu’il a parié. Implémentez maintenant l’enchère de Vickrey. Dans la classe AuctioneerAgent se trouve une fonction qui détermine le prix que paiera l’agent : computePrice. Modifiez cette fonction de manière à ce que le prix soit le second meilleur prix (ce qui correspond à l’enchère de Vickrey). Refaites les expériences précédentes. Que remarquez-vous ?

<!--Votre réponse-->
